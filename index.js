"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Context_1 = __importDefault(require("./App/Context"));
const Message_1 = __importDefault(require("./App/Message"));
exports.default = {
    AppInterface, Context: Context_1.default, Message: Message_1.default
};
