"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Message {
    constructor(destination, content, transporter) {
        this._destination = destination;
        this._content = JSON.stringify(content);
        this._transporter = content.constructor.name;
    }
    get destination() {
        return this._destination;
    }
    get content() {
        return this._content;
    }
    get transporter() {
        return this._transporter;
    }
}
exports.default = Message;
