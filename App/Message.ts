export default class Message{
        private readonly _destination: string;
        private readonly _content: string;
        private readonly _transporter: string;


        constructor(destination: string, content: object, transporter: string) {
            this._destination = destination;
            this._content = JSON.stringify(content);
            this._transporter = content.constructor.name;
        }


        get destination(): string {
            return this._destination;
        }

        get content(): string {
            return this._content;
        }

        get transporter(): string {
            return this._transporter;
        }
}