import Context from "./Context"
import Message from "./Message";

export default interface AppInterface{
        run(context: Context, log: object): Array<Message>
}